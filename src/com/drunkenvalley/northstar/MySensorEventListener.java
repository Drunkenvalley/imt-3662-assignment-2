package com.drunkenvalley.northstar;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class MySensorEventListener implements SensorEventListener{

	public MySensorEventListener () {
		// TODO: Nothing
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO: Nothing		
	}
	
	public float[] gravity;
	public float[] geomagnetic;
	float R[] = new float[9];
	float I[] = new float[9];
	public float orientation[] = new float[3]; 
	public OrientationInterface target = null;
	
	@Override
	public void onSensorChanged(SensorEvent event) {
		
	    // Get the data
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
	    	gravity = event.values;
	    if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
	    	geomagnetic = event.values;
	    
		// If we have the data
	    if (gravity != null && geomagnetic != null) {
	    	boolean success = SensorManager.getRotationMatrix(R, I, gravity, geomagnetic);
	    	if (success) {
	    		SensorManager.getOrientation(R, orientation);
	    		
	    		// If it can be output, send it
	    		if(target != null) {
	    			target.onNewOrientation(orientation, gravity);
	    		}
	        }
	    }
	}
}