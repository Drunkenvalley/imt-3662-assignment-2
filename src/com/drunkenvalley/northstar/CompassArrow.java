package com.drunkenvalley.northstar;

import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

public class CompassArrow {
	private static float currentAngle = 0.0f; 
	private static RotateAnimation compassArrowRotation; 
	private static ImageView compassImage;
	
	public static void init(ImageView compImg){
		compassImage = compImg;
	}
	
	public static void updateCompass(float angleUpdate){
		// To avoid the image taking the scenic route from 359� to 0�
		float angleMax = Math.max(currentAngle, -angleUpdate);
		float angleMin = Math.min(currentAngle, -angleUpdate);
		if(angleMax - angleMin > 180) {
			if(currentAngle > -angleUpdate)
				currentAngle -= 360;
			else
				currentAngle += 360;
		}
		
		compassArrowRotation = new RotateAnimation(currentAngle, -angleUpdate, 
				Animation.RELATIVE_TO_SELF, 0.5f, 
				Animation.RELATIVE_TO_SELF, 0.5f);
		
		compassArrowRotation.setDuration(50);
		compassArrowRotation.setFillAfter(true);
		
		compassImage.startAnimation(compassArrowRotation);
		currentAngle = -angleUpdate;
	}
}