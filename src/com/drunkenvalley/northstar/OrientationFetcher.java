package com.drunkenvalley.northstar;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public final class OrientationFetcher {
	
	// Data
	private static SensorManager sensorManager;
	private static Sensor accelerometer;
	private static Sensor magnetometer;
	private static MySensorEventListener eventListener;
	
	// Methods
	public static void init(Context context) {
		sensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
	    accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	    
	    eventListener = new MySensorEventListener();
	}
	
	public static void start() {
		sensorManager.registerListener(eventListener, accelerometer, SensorManager.SENSOR_DELAY_UI);
		sensorManager.registerListener(eventListener, magnetometer, SensorManager.SENSOR_DELAY_UI);
	}
	
	public static void stop() { 
		sensorManager.unregisterListener(eventListener);
	}
	
	public static void setOutputTarget(OrientationInterface target) {
		eventListener.target = target;
	}
}