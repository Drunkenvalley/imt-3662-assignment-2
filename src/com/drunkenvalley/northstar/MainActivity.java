package com.drunkenvalley.northstar;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.view.Display;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity implements OrientationInterface {

	TextView textMessage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		textMessage = ((TextView)findViewById(R.id.textViewMessage));
		CompassArrow.init((ImageView)findViewById(R.id.imageViewCompass));
		
		OrientationFetcher.init(getApplicationContext());
		OrientationFetcher.setOutputTarget(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void onNewOrientation(float[] orientations, float[] accelerometer) {
		// TODO Auto-generated method stub
		if(accelerometer[2] < 2.f){
			textMessage.setText(getResources().getString(R.string.errorMessage));
			CompassArrow.updateCompass(0.0f);
		}else{
			textMessage.setText("");
			
			float direction = (float)Math.floor(Math.toDegrees(orientations[0]));
			
			CompassArrow.updateCompass(direction);
		}
		
	}

	@Override
	public void onPause() {
		super.onPause();
		OrientationFetcher.stop();
	}

	@Override
	public void onResume() {
		super.onResume();

		OrientationFetcher.start();
	}
	
}
