package com.drunkenvalley.northstar;

public interface OrientationInterface {
	public abstract void onNewOrientation(float[] orientations, float[] accelerometer);
}